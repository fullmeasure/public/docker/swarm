FROM ubuntu:latest

MAINTAINER FME <dev@fullmeasureed.com>

# Install package
RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y monit redis-server nodejs libaio1 git curl ssh-client libgmp3-dev nano vim build-essential tmux cron ntp rsyslog tzdata && \
    rm -rf /var/lib/apt/lists/* && \
    mkdir -p /var/run/redis

# SETUP RUBY
ENV RUBY_VERSION 2.3.1
ENV RVM_BUNDLE /usr/local/rvm/wrappers/ruby-$RUBY_VERSION/bundle
ENV TERM=xterm-256color

# INSTALL RUBY
RUN command curl -sSL https://rvm.io/mpapis.asc | gpg --import - && \
    \curl -sSL https://get.rvm.io | bash -s stable && \
    /usr/local/rvm/bin/rvm install $RUBY_VERSION && \
    /usr/local/rvm/wrappers/ruby-$RUBY_VERSION/gem install bundler && \
    ln -s /usr/local/rvm/wrappers/ruby-$RUBY_VERSION/bundle /usr/bin/bundle

# SETUP CHECKOUT
COPY / /data/

WORKDIR /data/current

# SETUP SYSTEM
RUN mv /data/monitrc /etc/monit/monitrc && \
    chmod 700 /etc/monit/monitrc && \
    mv /data/*.conf /etc/monit/conf.d/ && \
    cp /usr/share/zoneinfo/US/Eastern /etc/localtime && \
    mv /data/cron_schedule /etc/cron.d/ && \
    chmod +x /data/*.sh
