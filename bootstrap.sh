#!/bin/bash

echo "" > /data/environment

for variable in `env`; do
  if [[ $variable != affinity:container* ]]; then
    echo "export $variable" >> /data/environment
  fi
done

echo "created environment file"

touch /etc/crontab /etc/cron.*/*

echo "purging old pid files"
rm -rf /var/run/*.pid
rm -rf /root/.monit.id
