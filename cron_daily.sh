#!/bin/bash

source /data/environment && cd /data/current && nice -n 1 $RVM_BUNDLE exec rake cron:all_daily &> /data/current/log/cron_daily.log
