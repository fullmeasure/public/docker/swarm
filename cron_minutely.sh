#!/bin/bash

source /data/environment && cd /data/current && nice -n 1 $RVM_BUNDLE exec rake cron:all_minutely &> /data/current/log/cron_minutely.log
