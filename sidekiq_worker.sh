#!/bin/bash

source /data/environment && cd /data/current && nice -n 11 $RVM_BUNDLE exec sidekiq -P /var/run/sidekiq.pid > log/sidekiq.log
